FROM openjdk:8-jdk-alpine

ENV SPRING_OUTPUT_ANSI_ENABLED=ALWAYS \
    JHIPSTER_SLEEP=0 \
    JAVA_OPTS=""

RUN rm -rf /srv/jhipster
# Create app directory
RUN mkdir -p /srv/jhipster
WORKDIR /srv/jhipster
ENV APP_HOME "/srv/jhipster"

COPY . /srv/jhipster

ADD ./entrypoint.sh /entrypoint.sh
RUN chmod +x ./entrypoint.sh
RUN chmod +x /entrypoint.sh

RUN ./mvnw -Pprod package

COPY . /srv/jhipster

RUN echo $(pwd)
RUN echo $(ls -lrt)

ADD /target/demoservice-0.0.1-SNAPSHOT.war /app.war

ENTRYPOINT ["/entrypoint.sh"]

EXPOSE 8081 5701/udp

