set -ex
echo "Running Docker"
docker login ${DOCKER_REGISTRY} -u ${DOCKER_USERNAME} -p ${DOCKER_PASSWORD}
docker build -t $DOCKER_REGISTRY/$CI_PROJECT_PATH:${CI_COMMIT_SHA:0:8} .
echo "Build Success"
docker build -t demo-project .

docker tag demo-project atanughosh/demo-project

docker push atanughosh/demo-project